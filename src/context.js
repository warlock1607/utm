import React from "react";
import { groups, items } from "./fakedata";

// Store Context is the global context that is managed by reducers.

const Store = React.createContext({
  groups,
  items
});

export default Store;
