export default function reducer(state, action) {
  switch (action.type) {
    case "COMPLETE":
      return {
        ...state,
        items: state.items.filter(t => t.id !== action.payload.id)
      };
    default:
      return state;
  }
}
