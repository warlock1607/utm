import React, { useContext } from "react";
import Store from "../context";
import { TodoHeader } from "./TodoHeader";

export default function TodoList({ groupID }) {
  const { state, dispatch } = useContext(Store);

  const tasks = state.items.filter(t => t.group === groupID)

  const pluralize = count =>
    count > 1 ? `There are ${count} todos.` : `There is ${count} todo.`;

  let header =
    state.items.length === 0 ? (
      <h4>Yay! All todos are done! Take a rest!</h4>
    ) : (
      <TodoHeader title={state.groups.filter(g => g.id === groupID)[0].title}>
        <span className="float-right">{pluralize(tasks.length)}</span>
      </TodoHeader>
    );
  return (
    <div style={{padding: `0 2vw`}}className="row">
      <div className="col-md-12">
        <div className="row">
          <div className="col-md-12">
            <br />
            {header}
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <ul className="list-group">
              {tasks.map(t => (
                <li key={t.id} className="list-group-item">
                  {t.title}
                  <button
                    className="float-right btn btn-danger btn-sm"
                    style={{ marginLeft: 10 }}
                    onClick={() => dispatch({ type: "COMPLETE", payload: t })}
                  >
                    Complete
                  </button>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
