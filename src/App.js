import React, { useContext, useReducer, useState } from "react";
import Timeline from "react-calendar-timeline";
import moment from "moment";
import { Drawer } from "@material-ui/core";

import TodoList from "./components/TodoList";

import "react-calendar-timeline/lib/Timeline.css";
import "./bootstrap.min.css";

import Store from "./context";
import reducer from "./reducer";

import { usePersistedContext, usePersistedReducer } from "./usePersist";

function App() {
  const globalStore = usePersistedContext(useContext(Store), "state");

  // `todos` will be a state manager to manage state.
  const [state, dispatch] = usePersistedReducer(
    useReducer(reducer, globalStore),
    "state" // The localStorage key
  );

  console.log(state);

  const [open, setOpen] = useState(false);
  const [targetGroupID, setTargetGroupID ] = useState(0);

  const toggleDrawer = e => setOpen(!open);

  const toggleTODO = id => {setTargetGroupID(state.items.filter(i => i.id === id)[0].group); toggleDrawer()}

  return (
    <Store.Provider value={{ state, dispatch }}>
      <Drawer open={open} onClose={toggleDrawer}>
        <TodoList groupID={targetGroupID}/>
      </Drawer>
      <Timeline
        onItemSelect={id => toggleTODO(id)}
        onItemClick={id => toggleTODO(id)}
        groups={state.groups}
        items={state.items}
        defaultTimeStart={moment().add(-12, "hour")}
        defaultTimeEnd={moment().add(12, "hour")}
      />
    </Store.Provider>
  );
}

export default App;
